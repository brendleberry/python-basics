---
marp: true
paginate: true

---
# Python for Data Analysis
Jonathan Brady

---
# Installation
Everything you'll need to get up and running:

* Python: https://www.python.org/downloads/
    * In the installer, make sure to enable the options to install Pip, and add Python to PATH
    * Pip is the package manager for Python, which lets you to download thirdparty packages easily
    * Adding Python to PATH lets you run Python from the command line in Windows

* Visual Studio Code: https://code.visualstudio.com/Download
    * Could use any other IDE or text editor, but VSCode is cross-platform, free, and actually good

---
# Hello world
Time to make our first program, which prints "Hello, world!" to the console

* Create a new text file in VSCode, and save it with the .py extension, e.g. "hello_world.py"

* At this point VSCode will probably prompt you to install the Python extension, which we want

* Add this code to the file:

```py
print("Hello, world!")
```

* Run our program by clicking the play button in the top-right, or pressing F5 or Ctrl-F5 to run with/without the debugger respectively

---
# Variables
To create a variable, we write a name, and assign a value to it

```py
string_to_print = "Hello, world!"
print(string_to_print)
```

Python is dynamically typed, which means we can change the type of a variable as we go

```py
thing_to_print = 5 # thing_to_print is an integer
print(thing_to_print)
print(type(thing_to_print))
thing_to_print = "Hello, world!" # thing_to_print is now a string
print(thing_to_print)
print(type(thing_to_print))
```

Avoid overusing this though, as it can make your code difficult to understand when you come back to it later

---
# Data types
Although Python is dynamically typed, so we don't really have to care about types while we are writing, objects do still have types under the hood. The built in types are:
* **Text type**: str
* **Numeric types**: int, float, complex
* **Sequence types**: 	list, tuple, range
* **Mapping type**: 	dict
* **Set types**: 	set, frozenset
* **Boolean type**: 	bool
* **Binary types**: 	bytes, bytearray, memoryview
* **None type**: 	NoneType 

---
# Operators
Operators are pieces of syntax that perform actions on variables
The behaviour of an operator will depend on the types of its operands, e.g. the "+" operator will perform addition for numeric types, but concatenation for strings
```py 
# x = 5
x = 1 + 4

# x = "banana"
x = "ban" + "ana"   
```

---
## Arithmetic operators
Does what it says on the tin - these are for doing arithmetic

```py
x = 5 + 2   # addition
x = 5 - 2   # subtraction
x = 5 * 2   # multiplication
x = 5 / 2   # division
x = 5 % 2   # modulo - this gives the remainder of 5 divided by 2
x = 5 ** 2  # exponentiation - this gives 5 squared
x = 5 // 2  # floor division - divides and ignores the remainder
```  

---
## Assignment operators
These operators are for assigning values to variables
Really, there is just the one "=" operator, and the others are shorthand for combining this with other operators 
```py
x = 5 # assigns 5 to x
x += 2 # equivalent to x = (x + 2)
x -= 2 # equivalent to x = (x - 2)
x *= 2 # equivalent to x = (x * 2)
x /= 2 # equivalent to x = (x / 2)
```

---
## Comparison operators
Again, does what it says on the tin - these operators are for comparing two objects

```py
x < y   # True if x is less than y
x <= y  # True if x is less than or equal to y
x > y   # True if x is greater than y
x >= y  # True if x is greater than or equal to y
x == y  # True if x is equal to y
x != y  # True if x is not equal to y 
``` 

---
## Logical operators
```py
x and y # True if x and y are both True
x or y  # True if either x or y is True
not x   # True if x is False
```

## Membership operator
The "in" operator tells us if a value or sequence can be found in a container
```py
5 in [1,2,3,4] # False
"pot" in "potato" # True  
```

---
## Index operator
Used for accessing elements of an ordered container, such as a list.
For a list with n elements, the first element has index 0, and the last element has index n-1.
However, we can also index into a container backwards, as the last element also has index -1, and the first element has index -n: 
```py
fruits = ["pear", "apple", "cherry"]
print(fruits[0])    # prints "pear"
print(fruits[1])    # prints "apple"
print(fruits[2])    # prints "cherry"
print(fruits[-1])   # prints "cherry"
print(fruits[-2])   # prints "apple"
print(fruits[-3])   # prints "pear"
```

---
# Containers
There are 4 main built-in containers in Python - list, tuple, set and dictionary.
Knowing the differences between them, and when to use each one, will prove useful.

---
## List
A list stores multiple objects in one variable.
It is ordered, allows duplicate values, and is mutable (its elements can be changed):
```py
fruits = ["pear", "apple", "cherry"]    # create a list
fruits[0] = "banana"                    # replace "pear" with "banana"
fruits.append("pineapple")              # add "pineapple" to the end of the list
fruits.insert(1, "kiwi")                # adds "kiwi" at index 1

print(fruits) # prints ["banana","kiwi","apple","cherry","pineapple"]

berries = ["strawberry", "blackberry", "raspberry"]
fruits.extend(berries) # adds all the elements in berries to fruits 
```
Be careful not to confuse append() and extend() - if you append() one list to another, it will add the entire list as a single element.

---
## List comprehension
Insanely powerful feature for generating lists from existing lists. The syntax is:
```py
new_list = [f(x) for x in container if condition]
```
Say we have a list of fruits, and we want to get all the fruits that aren't berries:
```py
fruits = ["pear", "apple", "cherry", "strawberry", "blackberry", "raspberry"]
berries = ["strawberry", "blackberry", "raspberry"]

non_berries = [f for f in fruits if f not in berries] 
print(non_berries) # prints ["pear", "apple", "cherry"]
```

---
## Tuple
Tuples are similar to lists in that they are ordered and allow duplicate values. However, they are immutable - this means we cannot add, change or remove values from a tuple once we have created it
```py
vegetables = ("broccoli", "cucumber", "tomato") # create a tuple
vegetables[0] = "pepper" # this line of code will throw an exception 
```

---
## Set
Sets are unordered containers that do not allow duplicate values (i.e. all elements in a set are unique).
The elements of a set are immutable, but elements can be added or removed.
You cannot access individual elements in a set by index or key, but you can loop over a set, or check if an element is contained in it:
```py
fruits = {"banana", "guava", "apricot"}

for fruit in fruits:
    # do something

if "guava" in fruits:
    # do something guava-specific
```
---
## Dictionary
The most useful container besides list. Dictionaries store elements in key-value pairs. The keys must be unique, but duplicate values are allowed.
```py
sam_fender_songs_and_listens = 
{
    "Seventeen Going Under" : 151_306_422, # underscores for readability
    "Hypersonic Missiles"   : 97_827_117,
    "Will We Talk?"         : 73_408_130,
    "Getting Started"       : 27_867_496,
    "Spit Of You"           : 35_609_433
}

print(sam_fender_songs_and_listens["Getting Started"]) # prints 27867496

# loop over the dictionary like so
for song, listens in sam_fender_songs_and_listens.items():
    # do something
```

---
# If statements
Executes one of two pieces of code based on a condition. Can chain them together to make an if, else-if, else statement.
In Python, "else if" is shortened to "elif":

```py
if (x > 10):
    print("x is greater than 10")
elif (x > 5):
    print("x is less than or equal to 10, but greater than 5")
else:
    print("x is less than 5")
```

Note: in the above example, the indentation isn't just to make things neat. Python expects you to indent your code after any line ending with a colon (e.g. if-statements, loops, function definitions)

---
# Loops
## While-loop
Loops over a block of code while a condition is True:
```py
# this loop will print 1, 2, 3, 4 and 5
count = 0 
while count < 5:
    count += 1
    print(count)
```

---
## Control flow in loops
We can use continue, break and else statements to skip to the next iteration of a loop, exit it entirely, or do something once the condition is no longer True:
```py
# this loop will print 1, 3, and 5
count = 0
while count < 10:
    count += 1 
    is_even = (count % 2 == 0)
    
    if (is_even):
        continue  # skip to the next iteration
    
    if (count == 7):
        break # exit the loop entirely

    print(count)

else:
    print("count reached 10!") # won't hit this if we exit at 7
```

---
## For-loop
For-loops are for iterating over containers:
```py
fruits = ["mango", "lime", "watermelon", "peach"]
for fruit in fruits:
    print(fruits)
```
They don't require an index variable, but if you want one you can just use the built-in enumerate() function:
```py
for i, fruit in enumerate(fruits):
    print(f"The {i}th fruit was {fruit}") 
```
In general, prefer using for-loops over while-loops - they are much safer as you don't have to remember to increment the loop counter properly.

---
# f-Strings
On the last slide, there was some interesting new syntax in the print statement - this was an f-String.
f-Strings let you put variables into strings and format them:
```py
name = "Fred Dibnah"
interests = ["chimneys", "steam", "Bolton"]
percentage_progress_through_slides = 12.3456789
print(f"My name is {name}")
print(f"I like {interests[0]}, {interests[1]}, and {interests[2]}")
print(f"I am {percentage_progress_through_slides:.2f}% of the way through these slides")
```
The ":" in the last line is a format specifier - the ".2f" indicates that we want the number to 2 decimal places. For more on f-String formatting:
https://www.pythontutorial.net/python-basics/python-f-strings/

---
# Functions
We can wrap up pieces of code that we want to reuse in a function, like so: 
```py
# calculate the mean of a list of numbers
def mean(numbers):
    sum = 0
    
    for number in numbers:
        sum += number

    count = len(numbers) # gives us the number of elements in the list
    return sum / count

```
---
# Functions (cont.)
Then, we can call that function multiple times, with different data, etc.
```py
sam_fender_spotify_listens = [
    151_306_422, 
    97_827_117, 
    73_408_130, 
    27_867_496, 
    35_609_433 ]

mean_sam_fender_spotify_listens = mean(sam_fender_spotify_listens)
print(mean_sam_fender_spotify_listens)
```
---
# Classes
Classes are another way of wrapping up bits of code that we want to reuse.
A class is an object that can contain data and/or functions (methods).
We can define a class like so:

```py
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def HaveBirthday(self):
        self.age += 1

jonathan = Person("Jonathan", 26)
jonathan.HaveBirthday()
print(jonathan.age) # prints 27

```

---
# Importing modules
Modules are essentially just Python files. When you import a module you can access any functions, classes or variables that are defined in that module. 
If we save our mean function from earlier to "mean.py", then we can import it in another Python file like so:
```py
import mean

some_data = [...] # stuff
mean.mean(some_data)
```

Note how we had to call "module.function" to get to our mean function.
We can actually import modules under a different name:
```py
import mean as some_other_name
some_other_name.mean(some_data)
```

---
# Importing modules (cont.)
Or just import the things we need from them:
```py
from mean import mean
mean(some_data)
```
There is one other thing we need to be aware of - when we import a module, we run the code in it. Really the only thing we want import to pick up is function and class definitions. We can prevent code from running on import by wrapping it like this:

```py
# function, class definitions and imports out here
if __name__ == "__main__":
    # stuff we don't want to run on import i.e. any actual work
```

---
# Reading/writing files
Let's say we have a .csv file we want to load and do some data analysis on.
First we'll need to read in the file:
```py
import os.path # these modules are both built-in
import csv

path_to_file = "test.csv"
if (os.path.exists(path_to_file)): # check the file exists first
    with open(path_to_file, "r") as f: # "r" opens the file in read mode
        reader = csv.reader(f)
        for row in reader:
            print(row)
```

---
# Thirdparty modules
## Installation
You can install thirdparty modules with Pip from the command line:
```sh
pip --version # check pip is installed first
pip install numpy
pip install pandas
pip install matplotlib
```

---
## Pandas
Pandas is a module that simplifies dealing with data, particularly reading and manipulating .csvs:

```py
import pandas as pd 

df = pd.read_csv("test.csv") # read in a csv
print(df["songs"].to_list()) # print the "songs" column
print(df["listens"].to_list()) # print the "listens" column
```

For more on Pandas (there is plenty): https://www.w3schools.com/python/pandas/default.asp

---
## NumPy
Module for dealing with arrays, matrices etc. Provides useful containers for n-dimensional arrays (and functions that act on them), which work much faster than the built-in lists.

```py
import numpy as np

my_array = np.array([[1,2,3],[4,5,6]]) # 2D array

# index into the array like so
print(my_array[0,2]) # prints 3
print(my_array[-1,1]) # prints 5
print(np.sum(my_array)) # sums over the whole array, prints 21
print(np.sum(my_array), axis=0) # sums over just the first axis, prints [5,7,9]
```

--- 
## Matplotlib
Library for plotting graphs - contains more features than I could hope to fit on a slide.
As a basic example let's plot a bar chart using the data we loaded with Pandas earlier:

```py
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("test.csv") # read in a csv
songs = df["songs"].to_list()
listens = df["listens"].to_list()

# hack to force the x-axis labels to wrap
wrapped_songs = [s.replace(' ', '\n') for s in songs]

plt.bar(wrapped_songs, listens)
plt.title("Sam Fender songs vs. no. Spotify listens")
plt.xlabel("Song")
plt.ylabel("No. Spotify listens")
plt.savefig("test.jpg")
plt.show()
```

---
![bg contain](Untitled.png)